/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include "include/window.hpp"
#include "include/renderdevice.hpp"


namespace oos {
    //
    // function declarations
    //

    RenderDevice *create_opengl_render_device(Window *in_window);
    RenderDevice *create_vulkan_render_device(Window *in_window);


    //
    // create_render_device
    //

    RenderDevice *create_render_device(Window *in_window, RenderDeviceType const in_type) {
        switch (in_type) {
        case RenderDeviceType::OpenGL:
            return(create_opengl_render_device(in_window));

        case RenderDeviceType::Vulkan:
            return(create_vulkan_render_device(in_window));
        }
        return(nullptr);
    }


    //
    // destroy_render_device
    //

    void destroy_render_device(RenderDevice *in_renderDevice) {
        delete in_renderDevice;
    }
}