/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>


namespace oos {
    namespace log {
        static std::shared_ptr<spdlog::logger>  s_logger;


        //
        // create
        //

        void create() {
            s_logger = spdlog::basic_logger_mt("engine", "engine.oos-log", true);
            s_logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%l] %v", spdlog::pattern_time_type::utc);
            s_logger->set_level(spdlog::level::info);
            s_logger->info("Log opened.");
        }


        //
        // destroy
        //

        void destroy() {
            if (nullptr != s_logger) {
                s_logger->info("Log closed.");
                s_logger->flush();
            }
            spdlog::shutdown();
        }


        //
        // flush
        //

        void flush() {
            s_logger->flush();
        }


        //
        // debug
        //

        void debug(std::string const &in_message) {
            if (nullptr != s_logger) {
                s_logger->debug(in_message);
            }
        }


        //
        // info
        //

        void info(std::string const &in_message) {
            if (nullptr != s_logger) {
                s_logger->info(in_message);
            }
        }


        //
        // warn
        //

        void warn(std::string const &in_message) {
            if (nullptr != s_logger) {
                s_logger->warn(in_message);
                s_logger->flush();
            }
        }


        //
        // critical
        //

        void critical(std::string const &in_message) {
            if (nullptr != s_logger) {
                s_logger->critical(in_message);
                s_logger->flush();
            }
        }
    }
}