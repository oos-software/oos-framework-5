/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include "include/framework.hpp"
#include "third-party/glfw/include/GLFW/glfw3.h"


#pragma comment(lib, "third-party/glfw/glfw3.lib")


namespace oos {
    // window
    Window *create_window(int const in_width, int const in_height, std::string const &in_title);
    void destroy_window(Window *in_window);

    // input_device
    InputDevice *create_input_device(Window *in_window);
    void destroy_input_device(InputDevice *in_inputDevice);

    // render device
    RenderDevice *create_render_device(Window *in_window, RenderDeviceType const in_type);
    void destroy_render_device(RenderDevice *in_renderDevice);


    // ======================================================================================
    //
    //  FrameworkImplementation
    //
    // ======================================================================================
    struct FrameworkImplementation : public Framework {
        // window
        Window *create_window(int const in_width, int const in_height, std::string const &in_title);
        void destroy_window(Window *in_window);

        // system device
        /*
        SystemDevice *create_system_device();
        
        */
        // input device
        InputDevice *create_input_device(Window *in_window);
        void destroy_input_device(InputDevice *in_inputDevice);

        // render device
        RenderDevice *create_render_device(Window *in_window);
        void destroy_render_device(RenderDevice *in_renderDevice);

        // audio device
        /*
        AudioDevice *create_audio_device();*/

        bool update(Window *in_window, std::function<void()> const &in_callback);
    };


    //
    // create_window
    //

    Window *FrameworkImplementation::create_window(int const in_width, int const in_height, std::string const &in_title) {
        return(::oos::create_window(in_width, in_height, in_title));
    }


    //
    // destroy_window
    //

    void FrameworkImplementation::destroy_window(Window *in_window) {
        return(::oos::destroy_window(in_window));
    }


    //
    // create_input_device
    //

    InputDevice *FrameworkImplementation::create_input_device(Window *in_window) {
        return(::oos::create_input_device(in_window));
    }


    //
    // destroy_input_device
    //

    void FrameworkImplementation::destroy_input_device(InputDevice *in_inputDevice) {
        ::oos::destroy_input_device(in_inputDevice);
    }


    //
    // create_render_device
    //

    RenderDevice *FrameworkImplementation::create_render_device(Window *in_window) {
        return(::oos::create_render_device(in_window, RenderDeviceType::OpenGL));  // TODO!
    }


    //
    // destroy_render_device
    //

    void FrameworkImplementation::destroy_render_device(RenderDevice *in_renderDevice) {
        ::oos::destroy_render_device(in_renderDevice);
    }


    //
    // update
    //

    bool FrameworkImplementation::update(Window *in_window, std::function<void()> const &in_callback) {
        auto  window = (GLFWwindow *)in_window->handle();
        glfwPollEvents();
        if (glfwWindowShouldClose(window)) {
            return(false);
        }
        in_callback();
        glfwSwapBuffers(window);
        return(true);
    }


    //
    // API
    //

    Framework *create_framework() {
        if (!glfwInit()) {
            return(nullptr);
        }
        return(new FrameworkImplementation());
    }

    void destroy_framework(Framework *in_framework) {
        delete static_cast<FrameworkImplementation *>(in_framework);
        glfwTerminate();
    }
}