/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include "../include/window.hpp"
#include "../include/renderdevice.hpp"


namespace oos {/*
    // ======================================================================================
    //
    //  VulkanRenderDevice
    //
    // ======================================================================================
    struct VulkanRenderDevice : public RenderDevice {
        
    };
    */

    //
    // create_vulkan_render_device
    //

    RenderDevice *create_vulkan_render_device(Window *in_window) {/*
        auto  rd = new VulkanRenderDevice();
        if (!rd->initialize(in_window)) {
            delete rd;
            return(nullptr);
        }
        return(rd);*/
        return(nullptr);
    }
}