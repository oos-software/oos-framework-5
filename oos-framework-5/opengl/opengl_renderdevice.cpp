/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <GL/gl3w.h>
#include "../third-party/glfw/include/GLFW/glfw3.h"

#include "../include/window.hpp"
#include "../include/renderdevice.hpp"

#include "opengl_buffer.hpp"
#include "opengl_renderpipeline.hpp"
#include "opengl_renderprogram.hpp"
#include "opengl_texture.hpp"
#include "opengl_vertexarray.hpp"


//#define OPENGL_DEBUGGING


namespace oos {
#ifdef OPENGL_DEBUGGING

    //
    // debug_message
    //

    static void APIENTRY debug_message(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
        printf("OpenGL debug message: %s\n", message);
        log::warn("OpenGL debug message: " + std::string(message));
    }

#endif


    // ======================================================================================
    //
    //  OpenGLRenderDevice
    //
    // ======================================================================================
    struct OpenGLRenderDevice : public RenderDevice {
        bool initialize(Window *in_window);

        void clear(bool const in_color, bool const in_depth, bool const in_stencil);

        // render pipeline
        RenderPipeline *create_render_pipeline(RenderPipelineSettings const &in_settings);
        void destroy_render_pipeline(RenderPipeline *in_renderPipeline);
        void set_render_pipeline(RenderPipeline *in_renderPipeline);

        // render program
        RenderProgram *create_render_program(RenderProgramSettings const &in_settings);
        void destroy_render_program(RenderProgram *in_renderProgram);
        void set_render_program(RenderProgram *in_renderProgram);
        void set_vector(int const in_location, int const in_numComponents, double const *in_values);
        void set_matrix(int const in_location, int const in_numComponents, double const *in_values);

        // vertex array
        VertexArray *create_vertex_array(VertexArraySettings const &in_settings);
        void destroy_vertex_array(VertexArray *in_vertexArray);

        // texture
        Texture *create_texture(TextureSettings const &in_settings);
        void destroy_texture(Texture *in_texture);
        void set_texture(int const in_unit, Texture *in_texture);

        // draw
        void draw_points(VertexArray *in_vertexArray, uint32_t const in_first, uint32_t const in_count);
        void draw_lines(VertexArray *in_vertexArray, uint32_t const in_first, uint32_t const in_count);
        void draw_triangles(VertexArray *in_vertexArray, uint32_t const in_first, uint32_t const in_count);
    };


    //
    // initialize
    //

    bool OpenGLRenderDevice::initialize(Window *in_window) {
        glfwMakeContextCurrent((GLFWwindow *)in_window->handle());
        
        if (GL3W_OK != gl3wInit()) {
            return(false);
        }
        if (!gl3wIsSupported(4, 5)) {
            //log::critical("OpenGL 4.5 not supported!");
            return(false);
        }

        glFrontFace(GL_CCW);
        return(true);
    }


    //
    // clear
    //

    void OpenGLRenderDevice::clear(bool const in_color, bool const in_depth, bool const in_stencil) {
        GLbitfield  mask = 0;

        if (in_color) {
            mask |= GL_COLOR_BUFFER_BIT;
        }
        if (in_depth) {
            mask |= GL_DEPTH_BUFFER_BIT;
        }
        if (in_stencil) {
            mask |= GL_STENCIL_BUFFER_BIT;
        }

        if (0 != mask) {
            glClear(mask);
        }
    }


    //
    // create_render_pipeline
    //

    RenderPipeline *OpenGLRenderDevice::create_render_pipeline(RenderPipelineSettings const &in_settings) {
        return(new OpenGLRenderPipeline(in_settings));
    }


    //
    // destroy_render_pipeline
    //

    void OpenGLRenderDevice::destroy_render_pipeline(RenderPipeline *in_renderPipeline) {
        delete static_cast<OpenGLRenderPipeline *>(in_renderPipeline);
    }


    //
    // set_render_pipeline
    //

    void OpenGLRenderDevice::set_render_pipeline(RenderPipeline *in_renderPipeline) {
        static_cast<OpenGLRenderPipeline *>(in_renderPipeline)->enable();
    }


    //
    // create_render_program
    //

    RenderProgram *OpenGLRenderDevice::create_render_program(RenderProgramSettings const &in_settings) {
        return(new OpenGLRenderProgram(in_settings));
    }


    //
    // destroy_render_program
    //

    void OpenGLRenderDevice::destroy_render_program(RenderProgram *in_renderProgram) {
        delete static_cast<OpenGLRenderProgram *>(in_renderProgram);
    }


    //
    // set_render_program
    //

    void OpenGLRenderDevice::set_render_program(RenderProgram *in_renderProgram) {
        static_cast<OpenGLRenderProgram *>(in_renderProgram)->enable();
    }


    //
    // set_vector
    //

    void OpenGLRenderDevice::set_vector(int const in_location, int const in_numComponents, double const *in_values) {
        if (in_numComponents < 2 || in_numComponents > 4) {
            return;
        }

        float  temp[4];
        for (int i = 0; i < in_numComponents; ++i) {
            temp[i] = float(in_values[i]);
        }

        switch (in_numComponents) {
        case 2:
            glUniform2fv(in_location, 1, temp);
            break;

        case 3:
            glUniform3fv(in_location, 1, temp);
            break;

        case 4:
            glUniform4fv(in_location, 1, temp);
            break;
        }
    }


    //
    // set_matrix
    //
    
    void OpenGLRenderDevice::set_matrix(int const in_location, int const in_numComponents, double const *in_values) {
        if (in_numComponents < 2 || in_numComponents > 4) {
            return;
        }

        float  temp[16];
        for (int i = 0; i < in_numComponents * in_numComponents; ++i) {
            temp[i] = float(in_values[i]);
        }

        switch (in_numComponents) {
        case 2:
            glUniformMatrix2fv(in_location, 1, GL_FALSE, temp);
            break;

        case 3:
            glUniformMatrix3fv(in_location, 1, GL_FALSE, temp);
            break;

        case 4:
            glUniformMatrix4fv(in_location, 1, GL_FALSE, temp);
            break;
        }
    }


    //
    // create_texture
    //

    Texture *OpenGLRenderDevice::create_texture(TextureSettings const &in_settings) {
        return(new OpenGLTexture(in_settings));
    }


    //
    // destroy_texture
    //

    void OpenGLRenderDevice::destroy_texture(Texture *in_texture) {
        delete static_cast<OpenGLTexture *>(in_texture);
    }


    //
    // set_texture
    //

    void OpenGLRenderDevice::set_texture(int const in_unit, Texture *in_texture) {
        static_cast<OpenGLTexture *>(in_texture)->bind(in_unit);
    }


    //
    // create_vertex_array
    //

    VertexArray *OpenGLRenderDevice::create_vertex_array(VertexArraySettings const &in_settings) {
        return(new OpenGLVertexArray(in_settings));
    }


    //
    // destroy_vertex_array
    //

    void OpenGLRenderDevice::destroy_vertex_array(VertexArray *in_vertexArray) {
        delete static_cast<OpenGLVertexArray *>(in_vertexArray);
    }


    //
    // draw_points
    //

    void OpenGLRenderDevice::draw_points(VertexArray *in_vertexArray, uint32_t const in_first, uint32_t const in_count) {
        auto  va = static_cast<OpenGLVertexArray *>(in_vertexArray);
        va->draw(GL_POINTS, in_first, in_count, true);
    }


    //
    // draw_lines
    //

    void OpenGLRenderDevice::draw_lines(VertexArray *in_vertexArray, uint32_t const in_first, uint32_t const in_count) {
        auto  va = static_cast<OpenGLVertexArray *>(in_vertexArray);
        va->draw(GL_LINES, in_first, in_count, true);
    }


    //
    // draw_triangles
    //

    void OpenGLRenderDevice::draw_triangles(VertexArray *in_vertexArray, uint32_t const in_first, uint32_t const in_count) {
        auto  va = static_cast<OpenGLVertexArray *>(in_vertexArray);
        va->draw(GL_TRIANGLES, in_first, in_count, true);
    }


    //
    // create_opengl_render_device
    //

    RenderDevice *create_opengl_render_device(Window *in_window) {
        auto  rd = new OpenGLRenderDevice();
        if (!rd->initialize(in_window)) {
            delete rd;
            return(nullptr);
        }
        return(rd);
    }
}