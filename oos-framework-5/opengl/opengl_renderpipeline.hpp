/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    // ======================================================================================
    //
    //  OpenGLRenderPipeline
    //
    // ======================================================================================
    struct OpenGLRenderPipeline : public RenderPipeline {
        OpenGLRenderPipeline(RenderPipelineSettings const &in_settings);
        
        void enable();


    private:
        RenderPipelineSettings  m_settings;
    };
}