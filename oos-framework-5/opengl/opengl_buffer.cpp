/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <GL/gl3w.h>

#include "../include/window.hpp"
#include "../include/renderdevice.hpp"

#include "opengl_buffer.hpp"


namespace oos {
    // ======================================================================================
    //
    //  OpenGLBuffer
    //
    // ======================================================================================

    //
    // constructor
    //

    OpenGLBuffer::OpenGLBuffer(GLenum const in_type, uint64_t const in_sizeInBytes, void const *in_data, bool const in_isDynamic) :
    m_type(in_type) {
        glGenBuffers(1, &m_handle);
        glBindBuffer(in_type, m_handle);
        glBufferData(in_type, in_sizeInBytes, in_data, in_isDynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
    }


    //
    // destructor
    //

    OpenGLBuffer::~OpenGLBuffer() {
        glDeleteBuffers(1, &m_handle);
    }


    //
    // bind
    //

    void OpenGLBuffer::bind() {
        glBindBuffer(m_type, m_handle);
    }


    //
    // map
    //

    void OpenGLBuffer::map(std::function<void(uint8_t *)> const &in_callback) {
        bind();
        void  *mapped = glMapBuffer(m_type, GL_WRITE_ONLY);
        if (nullptr != mapped) {
            in_callback(reinterpret_cast<uint8_t *>(mapped));
            glUnmapBuffer(m_type);
        }
    }
}