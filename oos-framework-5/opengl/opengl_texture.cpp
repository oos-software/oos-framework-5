/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <GL/gl3w.h>

#include "../include/renderdevice.hpp"
#include "opengl_texture.hpp"


namespace oos {
    //
    // target_for_settings
    //

    GLenum target_for_settings(TextureSettings const &in_settings) {/*
        switch (a_settings.type) {
        case 1: return(GL_TEXTURE_1D);
        case 2: return(GL_TEXTURE_2D);
        case 3: return(GL_TEXTURE_3D);
        }
        error("Invalid texture type!");
        return(0);*/
        return(GL_TEXTURE_2D);
    }


    //
    // internal_format_for_settings
    //

    static GLenum internal_format_for_settings(TextureSettings const &in_settings) {
        switch (in_settings.format) {
        case TextureFormat::UInt8:
            switch (in_settings.numComponents) {
            case 1: return(GL_R8);
            case 2: return(GL_RG8);
            case 3: return(GL_RGB8);
            case 4: return(GL_RGBA8);
            }
            break;

        case TextureFormat::Float16:
            switch (in_settings.numComponents) {
            case 1: return(GL_R16F);
            case 2: return(GL_RG16F);
            case 3: return(GL_RGB16F);
            case 4: return(GL_RGBA16F);
            }
            break;

        case TextureFormat::Float32:
            switch (in_settings.numComponents) {
            case 1: return(GL_R32F);
            case 2: return(GL_RG32F);
            case 3: return(GL_RGB32F);
            case 4: return(GL_RGBA32F);
            }
        }
        //error("Invalid settings for texture!");
        return(0);
    }


    //
    // format_for_settings
    //

    static GLenum format_for_settings(TextureSettings const &in_settings) {
        switch (in_settings.numComponents) {
        case 1: return(GL_RED);
        case 2: return(GL_RG);
        case 3: return(GL_RGB);
        case 4: return(GL_RGBA);
        }
        //error("Invalid settings for texture!");
        return(0);
    }


    //
    // type_for_settings
    //

    static GLenum type_for_settings(TextureSettings const &in_settings) {
        switch (in_settings.format) {
        case TextureFormat::UInt8:   return(GL_UNSIGNED_BYTE);
        case TextureFormat::Float16: return(GL_HALF_FLOAT);
        case TextureFormat::Float32: return(GL_FLOAT);
        }
        //error("Invalid settings for texture!");
        return(0);
    }


    //
    // native_wrap
    //

    static GLenum native_wrap(TextureWrap const in_wrap) {
        switch (in_wrap) {
        case TextureWrap::Repeat: return(GL_REPEAT);
        case TextureWrap::Clamp:  return(GL_CLAMP_TO_EDGE);
        }
        //error("Invalid wrap for texture!");
        return(0);
    }


    // ======================================================================================
    //
    //  OpenGLTexture
    //
    // ======================================================================================
    
    //
    // constructor
    //

    OpenGLTexture::OpenGLTexture(TextureSettings const &in_settings) :
    m_width(in_settings.width),
    m_height(in_settings.height),
    m_depth(in_settings.depth) {
        auto const  target = target_for_settings(in_settings);

        glGenTextures(1, &m_handle);
        glBindTexture(target, m_handle);

        set_filter(target, in_settings.filter);
        set_wrap(target, in_settings.wrap);

        auto const  internalFormat = internal_format_for_settings(in_settings);
        auto const  format = format_for_settings(in_settings);
        auto const  type = type_for_settings(in_settings);

        glTexImage2D(target, 0, internalFormat, m_width, m_height, 0, format, type, in_settings.data);
        //if (m_mipMap) {
            glGenerateMipmap(target);
        //}
    }


    //
    // destructor
    //

    OpenGLTexture::~OpenGLTexture() {
        glDeleteTextures(1, &m_handle);
    }


    //
    // bind
    //

    void OpenGLTexture::bind(int const in_unit) const {
        if (in_unit >= 0 && in_unit <= 7) {
            glActiveTexture(GL_TEXTURE0 + in_unit);
            glBindTexture(GL_TEXTURE_2D, m_handle);
        }
    }


    //
    // set_filter
    //

    void OpenGLTexture::set_filter(GLenum const in_target, TextureFilter const in_filter) {  // TODO: mip mapping
        auto const  magFilter = (TextureFilter::Nearest == in_filter ? GL_NEAREST : GL_LINEAR);
        auto const  minFilter = (TextureFilter::Nearest == in_filter ? GL_NEAREST_MIPMAP_NEAREST : GL_LINEAR_MIPMAP_LINEAR);
        
        glTexParameteri(in_target, GL_TEXTURE_MAG_FILTER, magFilter);
        glTexParameteri(in_target, GL_TEXTURE_MIN_FILTER, minFilter);

        if (TextureFilter::Anisotropic != in_filter) {
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 1.0f);
        }
        else {
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 8.0f);
        }
    }


    //
    // set_wrap
    //

    void OpenGLTexture::set_wrap(GLenum const in_target, TextureWrap const in_wrap) {
        auto const  native = native_wrap(in_wrap);
        glTexParameteri(in_target, GL_TEXTURE_WRAP_S, native);
        glTexParameteri(in_target, GL_TEXTURE_WRAP_T, native);
        glTexParameteri(in_target, GL_TEXTURE_WRAP_R, native);
    }
}