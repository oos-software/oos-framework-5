/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    // ======================================================================================
    //
    //  OpenGLTexture
    //
    // ======================================================================================
    struct OpenGLTexture : public Texture {
        OpenGLTexture(TextureSettings const &in_settings);
        ~OpenGLTexture();

        void bind(int const in_unit) const;


    private:
        void set_filter(GLenum const in_target, TextureFilter const in_filter);
        void set_wrap(GLenum const in_target, TextureWrap const in_wrap);


    private:
        GLuint     m_handle;

        int const  m_width;
        int const  m_height;
        int const  m_depth;
    };
}