/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    // ======================================================================================
    //
    //  OpenGLVertexBuffer
    //
    // ======================================================================================
    struct OpenGLVertexBuffer : public OpenGLBuffer {
        friend struct OpenGLVertexArray;


    public:
        OpenGLVertexBuffer(VertexArraySettings const &in_settings);

        //void write(uint32_t const in_firstVertex, uint32_t const in_numVertexes, void const *in_data);


    private:
        uint32_t const  m_stride;
        uint32_t const  m_numVertexes;
    };


    // ======================================================================================
    //
    //  OpenGLIndexBuffer
    //
    // ======================================================================================
    struct OpenGLIndexBuffer : public OpenGLBuffer {
        friend struct OpenGLVertexArray;


    public:
        OpenGLIndexBuffer(VertexArraySettings const &in_settings);

        //void write(uint32_t const in_firstIndex, uint32_t const in_numIndexes, void const *in_data);


    private:
        uint32_t const  m_numIndexes;
    };


    // ======================================================================================
    //
    //  OpenGLVertexArray
    //
    // ======================================================================================
    struct OpenGLVertexArray : public VertexArray {
        OpenGLVertexArray(VertexArraySettings const &in_settings);
        ~OpenGLVertexArray();

        void draw(GLenum const in_mode, uint32_t const in_first, uint32_t const in_count, bool const in_bind);


    private:
        GLuint              m_handle;
        OpenGLVertexBuffer  *m_vertexBuffer;
        OpenGLIndexBuffer   *m_indexBuffer;   // optional
    };
}