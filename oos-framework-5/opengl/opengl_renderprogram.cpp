/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <list>
#include <GL/gl3w.h>

#include "../include/renderdevice.hpp"
#include "opengl_renderprogram.hpp"


namespace oos {
    //
    // create_shader
    //

    static GLuint create_shader(GLenum const in_type, std::vector<uint8_t> const &in_code) {
        char const   *code = reinterpret_cast<char const *>(in_code.data());
        GLint const  size = GLint(in_code.size());

        auto const  handle = glCreateShader(in_type);
        glShaderSource(handle, 1, &code, &size);
        glCompileShader(handle);

        char  infoLog[512];
        glGetShaderInfoLog(handle, sizeof(infoLog), NULL, infoLog);

        if (0 != infoLog[0]) {
            GLint  success;
            glGetShaderiv(handle, GL_COMPILE_STATUS, &success);

            if (!success) {
                printf("%s\n", infoLog);
                //log::warn(fmt::format("Shader compiled with error(s): {}", infoLog));
            }
            else {
                printf("%s\n", infoLog);
                //log::warn(fmt::format("Shader compiled with warning(s): {}", infoLog));
            }
        }

        return(handle);
    }


    //
    // constructor
    //

    OpenGLRenderProgram::OpenGLRenderProgram(RenderProgramSettings const &in_settings) :
    m_handle(glCreateProgram()) {
        std::list<GLuint>  shaders;

        // create and compile all vertex shaders
        for (auto const &vs : in_settings.vertexShaders) {
            shaders.push_back(create_shader(GL_VERTEX_SHADER, vs));
        }

        // create and compile all fragment shaders
        for (auto const &fs : in_settings.fragmentShaders) {
            shaders.push_back(create_shader(GL_FRAGMENT_SHADER, fs));
        }

        // create and compile all compute shaders
        for (auto const &cs : in_settings.computeShaders) {
            shaders.push_back(create_shader(GL_COMPUTE_SHADER, cs));
        }

        // attach all shaders
        for (auto const shader : shaders) {
            glAttachShader(m_handle, shader);
        }

        // link and check the program
        GLint  success;
        glLinkProgram(m_handle);
        glGetProgramiv(m_handle, GL_LINK_STATUS, &success);
        if (!success) {
            char  infoLog[1024];
            glGetProgramInfoLog(m_handle, sizeof(infoLog), NULL, infoLog);
            //log::warn(fmt::format("Program link error: {}", infoLog));
        }

        // detach and delete all shaders (they are now inside the program)
        for (auto const shader : shaders) {
            glDetachShader(m_handle, shader);
            glDeleteShader(shader);
        }
    }


    //
    // destructor
    //

    OpenGLRenderProgram::~OpenGLRenderProgram() {
        glDeleteProgram(m_handle);
    }


    //
    // enable
    //

    void OpenGLRenderProgram::enable() {
        glUseProgram(m_handle);
    }
}