/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#define NOMINMAX
#include <GL/gl3w.h>
#include <algorithm>

#include "../include/renderdevice.hpp"
#include "opengl_buffer.hpp"
#include "opengl_vertexarray.hpp"


namespace oos {
    //
    // native_vertex_format
    //

    static GLenum native_vertex_format(VertexFormat const in_format) {
        switch (in_format) {
        case VertexFormat::Float32:
            return(GL_FLOAT);
        }
        //error("Invalid vertex format!");
        return(0);
    }


    //
    // size_for_vertex_format
    //

    static GLsizei size_for_vertex_format(VertexFormat const in_format) {
        switch (in_format) {
        case VertexFormat::Float32:
            return(4);
        }
        //error("Invalid vertex format!");
        return(0);
    }


    //
    // stride_from_vertex_attributes
    //

    static GLsizei stride_from_vertex_attributes(std::vector<std::pair<int, VertexFormat>> const &in_attributes) {
        GLsizei  stride = 0;
        for (auto const &attrib : in_attributes) {
            stride += attrib.first * size_for_vertex_format(attrib.second);
        }
        return(stride);
    }


    // ======================================================================================
    //
    //  OpenGLVertexBuffer
    //
    // ======================================================================================
    
    //
    // constructor
    //

    OpenGLVertexBuffer::OpenGLVertexBuffer(VertexArraySettings const &in_settings) :
    OpenGLBuffer(GL_ARRAY_BUFFER, uint64_t(in_settings.numVertexes) * uint64_t(stride_from_vertex_attributes(in_settings.attributes)), in_settings.vertexes, in_settings.isDynamic),
    m_stride(stride_from_vertex_attributes(in_settings.attributes)),
    m_numVertexes(in_settings.numVertexes) {
        GLuint    index = 0;
        uint64_t  offset = 0;

        for (auto const &attrib : in_settings.attributes) {
            glEnableVertexAttribArray(index);
            glVertexAttribPointer(index++, attrib.first, native_vertex_format(attrib.second), GL_FALSE, m_stride, reinterpret_cast<void const *>(offset));
            offset += uint64_t(attrib.first) * uint64_t(size_for_vertex_format(attrib.second));
        }
    }

    /*
    //
    // write
    //

    void VertexBuffer::write(uint32_t const in_firstVertex, uint32_t const in_numVertexes, void const *in_data) {
        map([this, in_firstVertex, in_numVertexes, in_data](uint8_t *ptr) {
            if (in_firstVertex < m_numVertexes) {
                memcpy(&ptr[uint64_t(in_firstVertex) * uint64_t(m_stride)], in_data, uint64_t(std::min(m_numVertexes - in_firstVertex, in_numVertexes)) * uint64_t(m_stride));
            }
        });
    }
    */

    // ======================================================================================
    //
    //  OpenGLIndexBuffer
    //
    // ======================================================================================
    
    //
    // constructor
    //

    OpenGLIndexBuffer::OpenGLIndexBuffer(VertexArraySettings const &in_settings) :
    OpenGLBuffer(GL_ELEMENT_ARRAY_BUFFER, in_settings.numIndexes * sizeof(uint32_t), in_settings.indexes, in_settings.isDynamic),
    m_numIndexes(in_settings.numIndexes) {

    }

    /*
    //
    // write
    //

    void IndexBuffer::write(uint32_t const in_firstIndex, uint32_t const in_numIndexes, void const *in_data) {
        map([this, in_firstIndex, in_numIndexes, in_data](uint8_t *ptr) {
            if (in_firstIndex < m_numIndexes) {
                memcpy(&ptr[in_firstIndex * sizeof(uint32_t)], in_data, std::min(m_numIndexes - in_firstIndex, in_numIndexes) * sizeof(uint32_t));
            }
        });
    }
    */

    // ======================================================================================
    //
    //  OpenGLVertexArray
    //
    // ======================================================================================
    
    //
    // constructor
    //

    OpenGLVertexArray::OpenGLVertexArray(VertexArraySettings const &in_settings) :
    m_vertexBuffer(nullptr),
    m_indexBuffer(nullptr) {
        glGenVertexArrays(1, &m_handle);
        glBindVertexArray(m_handle);
    
        m_vertexBuffer = new OpenGLVertexBuffer(in_settings);

        if (in_settings.numIndexes > 0 && nullptr != in_settings.indexes) {
            m_indexBuffer = new OpenGLIndexBuffer(in_settings);
        }
    }


    //
    // destructor
    //

    OpenGLVertexArray::~OpenGLVertexArray() {
        delete m_vertexBuffer;
        delete m_indexBuffer;
        glDeleteVertexArrays(1, &m_handle);
    }


    //
    // draw
    //

    void OpenGLVertexArray::draw(GLenum const in_mode, uint32_t const in_first, uint32_t const in_count, bool const in_bind) {
        if (in_bind) {
            glBindVertexArray(m_handle);
        }

        if (nullptr == m_indexBuffer) {
            glDrawArrays(in_mode, in_first, std::min(m_vertexBuffer->m_numVertexes - in_first, in_count));
        }
        else {
            glDrawElements(in_mode, std::min(m_indexBuffer->m_numIndexes - in_first, in_count), GL_UNSIGNED_INT, reinterpret_cast<void const *>(in_first * sizeof(uint32_t)));
        }
    }
}