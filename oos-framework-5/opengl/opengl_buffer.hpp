/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <functional>


namespace oos {
    // ======================================================================================
    //
    //  OpenGLBuffer
    //
    // ======================================================================================
    struct OpenGLBuffer {
        OpenGLBuffer(GLenum const in_type, uint64_t const in_sizeInBytes, void const *in_data, bool const in_isDynamic);
        ~OpenGLBuffer();

        void bind();


    protected:
        void map(std::function<void(uint8_t *)> const &in_callback);


    private:
        GLenum const  m_type;
        GLuint        m_handle;
    };
}