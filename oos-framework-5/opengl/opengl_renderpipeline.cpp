/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <GL/gl3w.h>

#include "../include/renderdevice.hpp"
#include "opengl_renderpipeline.hpp"


namespace oos {
    // ======================================================================================
    //
    //  OpenGLRenderPipeline
    //
    // ======================================================================================
    
    //
    // constructor
    //

    OpenGLRenderPipeline::OpenGLRenderPipeline(RenderPipelineSettings const &in_settings) :
    m_settings(in_settings) {

    }


    //
    // enable
    //

    void OpenGLRenderPipeline::enable() {
        // color
        glColorMask(m_settings.redMask, m_settings.greenMask, m_settings.blueMask, m_settings.alphaMask);

        // depth
        switch (m_settings.depthMode) {
        case DepthMode::Less:
            glDepthFunc(GL_LESS);
            break;

        case DepthMode::LessEqual:
            glDepthFunc(GL_LEQUAL);
            break;

        case DepthMode::Greater:
            glDepthFunc(GL_GREATER);
            break;

        case DepthMode::GreaterEqual:
            glDepthFunc(GL_GEQUAL);
            break;

        case DepthMode::Always:
            glDepthFunc(GL_ALWAYS);
            break;

        case DepthMode::Never:
            glDepthFunc(GL_NEVER);
            break;
        }

        glDepthMask(m_settings.depthMask);

        if (m_settings.depthTest) {
            glEnable(GL_DEPTH_TEST);
        }
        else {
            glDisable(GL_DEPTH_TEST);
        }

        // stencil


        // culling
        switch (m_settings.cullMode) {
        case CullMode::None:
            glDisable(GL_CULL_FACE);
            break;

        case CullMode::Front:
            glCullFace(GL_FRONT);
            glEnable(GL_CULL_FACE);
            break;

        case CullMode::Back:
            glCullFace(GL_BACK);
            glEnable(GL_CULL_FACE);
            break;
        }
    }
}