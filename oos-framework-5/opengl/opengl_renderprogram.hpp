/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    // ======================================================================================
    //
    //  OpenGLRenderProgram
    //
    // ======================================================================================
    struct OpenGLRenderProgram : public RenderProgram {
        OpenGLRenderProgram(RenderProgramSettings const &in_settings);
        ~OpenGLRenderProgram();

        void enable();


    private:
        GLuint  m_handle;
    };
}