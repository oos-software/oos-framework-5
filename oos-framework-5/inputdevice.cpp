/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include "include/window.hpp"
#include "include/inputdevice.hpp"

#include "third-party/glfw/include/GLFW/glfw3.h"


namespace oos {
    static int constexpr  MaxInputEvents = 32;
    
    
    // ======================================================================================
    //
    //  InputDeviceImplementation
    //
    // ======================================================================================
    struct InputDeviceImplementation : public InputDevice {
        InputDeviceImplementation(Window *in_window) :
        m_window(in_window),
        m_numEvents(0) {
            if (glfwRawMouseMotionSupported()) {
                auto  window = (GLFWwindow *)in_window->handle();
                glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
            }
        }

        //
        // enable_cursor
        //

        void enable_cursor() {
            auto  window = (GLFWwindow *)m_window->handle();
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }

        //
        // disable_cursor
        //

        void disable_cursor() {
            auto  window = (GLFWwindow *)m_window->handle();
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }

        //
        // get_cursor_position
        //

        std::pair<double, double> get_cursor_position() const {
            double  x, y;
            auto    window = (GLFWwindow *)m_window->handle();
            glfwGetCursorPos(window, &x, &y);
            return { x, y };
        }

        //
        // set_cursor_position
        //

        void set_cursor_position(double const in_x, double const in_y) {
            auto  window = (GLFWwindow *)m_window->handle();
            glfwSetCursorPos(window, in_x, in_y);
        }

        //
        // add_event
        //

        void add_event(InputEvent const &in_event) {
            if (m_numEvents >= MaxInputEvents) {
                return;
            }
            m_events[m_numEvents++] = in_event;
        }

        //
        // process_events
        //

        void process_events(std::function<void(InputEvent const &)> const &in_handler) {
            for (int i = 0; i < m_numEvents; ++i) {
                in_handler(m_events[i]);
            }
            m_numEvents = 0;
        }


    private:
        Window      *m_window;

        int         m_numEvents;
        InputEvent  m_events[MaxInputEvents];
    };


    static InputDeviceImplementation  *s_inputDevice;
    static InputKey                   s_inputKeyMapping[GLFW_KEY_LAST + 1];


    //
    // key_callback
    //

    static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
        auto const  ik = s_inputKeyMapping[key];

        if (InputKey::None == ik) {
            return;
        }

        InputEvent  ev;
        ev.key = ik;
        ev.value = 0.0;

        if (GLFW_PRESS == action) {
            ev.action = InputAction::Press;
            s_inputDevice->add_event(ev);
        }
        else if (GLFW_RELEASE == action) {
            ev.action = InputAction::Release;
            s_inputDevice->add_event(ev);
        }
    }


    //
    // mouse_button_callback
    //

    static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
        InputEvent  ev;
        ev.value = 0.0;

        for (int b = 0; b < 8; ++b) {
            auto const  native = GLFW_MOUSE_BUTTON_1 + b;
            if (button == native) {
                ev.key = InputKey(int(InputKey::Mouse_0) + b);
                if (GLFW_PRESS == action) {
                    ev.action = InputAction::Press;
                    s_inputDevice->add_event(ev);
                }
                else if (GLFW_RELEASE == action) {
                    ev.action = InputAction::Release;
                    s_inputDevice->add_event(ev);
                }
            }
        }
    }


    //
    // joystick_callback
    //

    static void josytick_callback() {

    }


    //
    // initialize_key_mappings
    //

    static void initialize_key_mappings() {
        for (int i = 0; i <= GLFW_KEY_LAST; ++i) {
            s_inputKeyMapping[i] = InputKey::None;
        }

        s_inputKeyMapping[GLFW_KEY_ESCAPE] = InputKey::Escape;
        s_inputKeyMapping[GLFW_KEY_SPACE] = InputKey::Space;
        s_inputKeyMapping[GLFW_KEY_ENTER] = InputKey::Enter;

        // F1-F12
        s_inputKeyMapping[GLFW_KEY_F1] = InputKey::F1;
        s_inputKeyMapping[GLFW_KEY_F2] = InputKey::F2;
        s_inputKeyMapping[GLFW_KEY_F3] = InputKey::F3;
        s_inputKeyMapping[GLFW_KEY_F4] = InputKey::F4;
        s_inputKeyMapping[GLFW_KEY_F5] = InputKey::F5;
        s_inputKeyMapping[GLFW_KEY_F6] = InputKey::F6;
        s_inputKeyMapping[GLFW_KEY_F7] = InputKey::F7;
        s_inputKeyMapping[GLFW_KEY_F8] = InputKey::F8;
        s_inputKeyMapping[GLFW_KEY_F9] = InputKey::F9;
        s_inputKeyMapping[GLFW_KEY_F10] = InputKey::F10;
        s_inputKeyMapping[GLFW_KEY_F11] = InputKey::F11;
        s_inputKeyMapping[GLFW_KEY_F12] = InputKey::F12;

        // letters (a-z)
        s_inputKeyMapping[GLFW_KEY_A] = InputKey::A;
        s_inputKeyMapping[GLFW_KEY_B] = InputKey::B;
        s_inputKeyMapping[GLFW_KEY_C] = InputKey::C;
        s_inputKeyMapping[GLFW_KEY_D] = InputKey::D;
        s_inputKeyMapping[GLFW_KEY_E] = InputKey::E;
        s_inputKeyMapping[GLFW_KEY_F] = InputKey::F;
        s_inputKeyMapping[GLFW_KEY_G] = InputKey::G;
        s_inputKeyMapping[GLFW_KEY_H] = InputKey::H;
        s_inputKeyMapping[GLFW_KEY_I] = InputKey::I;
        s_inputKeyMapping[GLFW_KEY_J] = InputKey::J;
        s_inputKeyMapping[GLFW_KEY_K] = InputKey::K;
        s_inputKeyMapping[GLFW_KEY_L] = InputKey::L;
        s_inputKeyMapping[GLFW_KEY_M] = InputKey::M;
        s_inputKeyMapping[GLFW_KEY_N] = InputKey::N;
        s_inputKeyMapping[GLFW_KEY_O] = InputKey::O;
        s_inputKeyMapping[GLFW_KEY_P] = InputKey::P;
        s_inputKeyMapping[GLFW_KEY_Q] = InputKey::Q;
        s_inputKeyMapping[GLFW_KEY_R] = InputKey::R;
        s_inputKeyMapping[GLFW_KEY_S] = InputKey::S;
        s_inputKeyMapping[GLFW_KEY_T] = InputKey::T;
        s_inputKeyMapping[GLFW_KEY_U] = InputKey::U;
        s_inputKeyMapping[GLFW_KEY_V] = InputKey::V;
        s_inputKeyMapping[GLFW_KEY_W] = InputKey::W;
        s_inputKeyMapping[GLFW_KEY_X] = InputKey::X;
        s_inputKeyMapping[GLFW_KEY_Y] = InputKey::Y;
        s_inputKeyMapping[GLFW_KEY_Z] = InputKey::Z;

        // digits (0-9)
        s_inputKeyMapping[GLFW_KEY_0] = InputKey::Zero;
        s_inputKeyMapping[GLFW_KEY_1] = InputKey::One;
        s_inputKeyMapping[GLFW_KEY_2] = InputKey::Two;
        s_inputKeyMapping[GLFW_KEY_3] = InputKey::Three;
        s_inputKeyMapping[GLFW_KEY_4] = InputKey::Four;
        s_inputKeyMapping[GLFW_KEY_5] = InputKey::Five;
        s_inputKeyMapping[GLFW_KEY_6] = InputKey::Six;
        s_inputKeyMapping[GLFW_KEY_7] = InputKey::Seven;
        s_inputKeyMapping[GLFW_KEY_8] = InputKey::Eight;
        s_inputKeyMapping[GLFW_KEY_9] = InputKey::Nine;
    }


    //
    // create_input_device
    //

    InputDevice *create_input_device(Window *in_window) {
        initialize_key_mappings();

        auto  window = (GLFWwindow *)in_window->handle();
        glfwSetKeyCallback(window, key_callback);
        glfwSetMouseButtonCallback(window, mouse_button_callback);
        //glfwSetJoystickCallback(window, joystick_callback);

        s_inputDevice = new InputDeviceImplementation(in_window);
        return(s_inputDevice);
    }


    //
    // destroy_input_device
    //

    void destroy_input_device(InputDevice *in_inputDevice) {
        s_inputDevice = nullptr;
        delete static_cast<InputDeviceImplementation *>(in_inputDevice);
    }
}