/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include "include/window.hpp"

#include "third-party/glfw/include/GLFW/glfw3.h"


namespace oos {
    // ======================================================================================
    //
    //  WindowImplementation
    //
    // ======================================================================================
    struct WindowImplementation : public Window {
        void *handle() {
            return(m_native);
        }

        int width() const {
            int  width, height;
            glfwGetWindowSize(m_native, &width, &height);
            return(width);
        }

        int height() const {
            int  width, height;
            glfwGetWindowSize(m_native, &width, &height);
            return(height);
        }

        std::string const &title() const {
            return(m_title);
        }

        void title(std::string const &in_title) {
            m_title = in_title;
            glfwSetWindowTitle(m_native, in_title.c_str());
        }

        void show() {
            glfwShowWindow(m_native);
        }


    public:
        bool create(int const in_width, int const in_height, std::string const &in_title) {
            m_title = in_title;
            m_native = glfwCreateWindow(in_width, in_height, in_title.c_str(), NULL, NULL);
            return(nullptr != m_native);
        }


    private:
        GLFWwindow   *m_native;
        std::string  m_title;
    };

    
    //
    // create_window
    //

    Window *create_window(int const in_width, int const in_height, std::string const &in_title) {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        
        glfwWindowHint(GLFW_RED_BITS, 8);
        glfwWindowHint(GLFW_GREEN_BITS, 8);
        glfwWindowHint(GLFW_BLUE_BITS, 8);
        glfwWindowHint(GLFW_ALPHA_BITS, 8);
        glfwWindowHint(GLFW_DEPTH_BITS, 24);
        glfwWindowHint(GLFW_STENCIL_BITS, 8);

        glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);

        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        
        //glfwSetWindowSizeLimits((GLFWwindow *)s_engine.window->handle(), MinWidth, MinHeight, GLFW_DONT_CARE, GLFW_DONT_CARE);

        auto  impl = new WindowImplementation();
        if (!impl->create(in_width, in_height, in_title)) {
            delete impl;
            return(nullptr);
        }
        return(impl);
    }


    //
    // destroy_window
    //

    void destroy_window(Window *in_window) {
        delete static_cast<WindowImplementation *>(in_window);
    }
}