/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <functional>

#include "window.hpp"
#include "systemdevice.hpp"
#include "inputdevice.hpp"
#include "renderdevice.hpp"
#include "audiodevice.hpp"


namespace oos {
    // ======================================================================================
    //
    //  Framework
    //
    // ======================================================================================
    struct Framework {
        virtual Window *create_window(int const in_width, int const in_height, std::string const &in_title) = 0;
        virtual void destroy_window(Window *in_window) = 0;
        /*
        virtual SystemDevice *create_system_device() = 0;
        virtual void destroy_system_device(SystemDevice *in_systemDevice) = 0;
        */
        virtual InputDevice *create_input_device(Window *in_window) = 0;
        virtual void destroy_input_device(InputDevice *in_inputDevice) = 0;
        
        virtual RenderDevice *create_render_device(Window *in_window) = 0;
        virtual void destroy_render_device(RenderDevice *in_renderDevice) = 0;
        /*
        virtual AudioDevice *create_audio_device() = 0;
        virtual void destroy_audio_device(AudioDevice *in_audioDevice) = 0;*/

        virtual bool update(Window *in_window, std::function<void()> const &in_callback) = 0;
    };


    //
    // API
    //

    __declspec(dllexport) Framework *create_framework();
    __declspec(dllexport) void destroy_framework(Framework *in_framework);
}