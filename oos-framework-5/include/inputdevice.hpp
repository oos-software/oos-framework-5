/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <functional>


namespace oos {
    // ======================================================================================
    //
    //  InputKey
    //
    // ======================================================================================
    enum class InputKey {
        None,

        Escape,
        Space,
        Enter,

        // F1-F12
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        
        // letters (a-z)
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,
        
        // digits (0-9)
        Zero,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,

        // mouse axes must be lienar inside the table
        Mouse_X_Minus,
        Mouse_X_Plus,
        Mouse_Y_Minus,
        Mouse_Y_Plus,

        // mouse buttons must be linear inside the table
        Mouse_0,
        Mouse_1,
        Mouse_2,
        Mouse_3,
        Mouse_4,
        Mouse_5,
        Mouse_6,
        Mouse_7,

        // mouse wheel
        MouseWheel_Vertical,
        MouseWheel_Horizontal,

        // joystick axes must bei linear inside the table
        Joystick_X_Minus,
        Joystick_X_Plus,
        Joystick_Y_Minus,
        Joystick_Y_Plus,

        // joystick buttons must be linear inside the table
        Joystick_0,
        Joystick_1,
        Joystick_2,
        Joystick_3,
        Joystick_4,
        Joystick_5,
        Joystick_6,
        Joystick_7,
        Joystick_8,
        Joystick_9,
        Joystick_10,
        Joystick_11,
        Joystick_12,
        Joystick_13,
        Joystick_14,
        Joystick_15,

        NumInputKeys
    };
    

    // ======================================================================================
    //
    //  InputAction
    //
    // ======================================================================================
    enum class InputAction {
        Press,
        Release
    };


    // ======================================================================================
    //
    //  InputEvent
    //
    // ======================================================================================
    struct InputEvent {
        InputKey     key;
        InputAction  action;
        double       value;    // value for axis or mouse wheel
    };


    // ======================================================================================
    //
    //  InputDevice
    //
    // ======================================================================================
    struct InputDevice {
        virtual void enable_cursor() = 0;
        virtual void disable_cursor() = 0;

        virtual std::pair<double, double> get_cursor_position() const = 0;
        virtual void set_cursor_position(double const in_x, double const in_y) = 0;

        virtual void process_events(std::function<void(InputEvent const &)> const &in_handler) = 0;
    };
}