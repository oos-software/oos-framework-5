/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <vector>


namespace oos {
    // ======================================================================================
    //
    //  RenderDeviceType
    //
    // ======================================================================================
    enum class RenderDeviceType {
        OpenGL,
        Vulkan
    };


    // ======================================================================================
    //
    //  DepthMode
    //
    // ======================================================================================
    enum class DepthMode {
        Less,
        LessEqual,
        Greater,
        GreaterEqual,
        Always,
        Never
    };


    // ======================================================================================
    //
    //  StencilMode
    //
    // ======================================================================================
    enum class StencilMode {

    };


    // ======================================================================================
    //
    //  CullMode
    //
    // ======================================================================================
    enum class CullMode {
        None,
        Front,
        Back
    };


    // ======================================================================================
    //
    //  RenderPipelineSettings
    //
    // ======================================================================================
    struct RenderPipelineSettings {
        bool         redMask = true;
        bool         greenMask = true;
        bool         blueMask = true;
        bool         alphaMask = true;

        DepthMode    depthMode = DepthMode::Less;
        bool         depthMask = true;
        bool         depthTest = true;

        StencilMode  stencilMode;
        bool         stencilTest = false;

        CullMode     cullMode = CullMode::Back;
    };


    // ======================================================================================
    //
    //  RenderPipeline
    //
    // ======================================================================================
    struct RenderPipeline {

    };


    // ======================================================================================
    //
    //  RenderProgramSettings
    //
    // ======================================================================================
    struct RenderProgramSettings {
        std::vector<std::vector<uint8_t>>  vertexShaders;
        std::vector<std::vector<uint8_t>>  fragmentShaders;
        std::vector<std::vector<uint8_t>>  computeShaders;
    };


    // ======================================================================================
    //
    //  RenderProgram
    //
    // ======================================================================================
    struct RenderProgram {
        
    };


    // ======================================================================================
    //
    //  TextureFormat
    //
    // ======================================================================================
    enum class TextureFormat {
        UInt8,
        Float16,
        Float32
    };


    // ======================================================================================
    //
    //  TextureFilter
    //
    // ======================================================================================
    enum class TextureFilter {
        Nearest,
        Linear,
        Anisotropic
    };


    // ======================================================================================
    //
    //  TextureWrap
    //
    // ======================================================================================
    enum class TextureWrap {
        Repeat,
        Clamp
    };


    // ======================================================================================
    //
    //  TextureCompression
    //
    // ======================================================================================
    enum class TextureCompression {
        None,
        BC7
    };


    // ======================================================================================
    //
    //  TextureSettings
    //
    // ======================================================================================
    struct TextureSettings {
        int                 width = 1;
        int                 height = 1;
        int                 depth = 1;
        int                 numComponents = 4;
        TextureFormat       format = TextureFormat::UInt8;
        TextureFilter       filter = TextureFilter::Linear;
        TextureWrap         wrap = TextureWrap::Repeat;
        TextureCompression  compression = TextureCompression::None;
        void const          *data = nullptr;
    };


    // ======================================================================================
    //
    //  Texture
    //
    // ======================================================================================
    struct Texture {/*
        virtual int width() const = 0;
        virtual int height() const = 0;
        virtual int depth() const = 0;*/
    };


    // ======================================================================================
    //
    //  VertexFormat
    //
    // ======================================================================================
    enum class VertexFormat {
        Float32
    };


    // ======================================================================================
    //
    //  VertexArraySettings
    //
    // ======================================================================================
    struct VertexArraySettings {
        std::vector<std::pair<int, VertexFormat>>  attributes;
        uint32_t                                   numVertexes;
        void const                                 *vertexes;
        uint32_t                                   numIndexes;
        uint32_t const                             *indexes;
        bool                                       isDynamic = false;
    };
    

    // ======================================================================================
    //
    //  VertexArray
    //
    // ======================================================================================
    struct VertexArray {

    };


    // ======================================================================================
    //
    //  RenderDevice
    //
    // ======================================================================================
    struct RenderDevice {
        virtual ~RenderDevice() = default;

        virtual void clear(bool const in_color, bool const in_depth, bool const in_stencil) = 0;
        
        // render pipeline
        virtual RenderPipeline *create_render_pipeline(RenderPipelineSettings const &in_settings) = 0;
        virtual void destroy_render_pipeline(RenderPipeline *in_renderPipeline) = 0;
        virtual void set_render_pipeline(RenderPipeline *in_renderPipeline) = 0;

        // render program
        virtual RenderProgram *create_render_program(RenderProgramSettings const &in_settings) = 0;
        virtual void destroy_render_program(RenderProgram *in_renderProgram) = 0;
        virtual void set_render_program(RenderProgram *in_renderProgram) = 0;
        virtual void set_vector(int const in_location, int const in_numComponents, double const *in_values) = 0;
        virtual void set_matrix(int const in_location, int const in_numComponents, double const *in_values) = 0;

        // texture
        virtual Texture *create_texture(TextureSettings const &in_settings) = 0;
        virtual void destroy_texture(Texture *in_texture) = 0;
        //virtual void update_texture(void const *in_data) = 0;
        virtual void set_texture(int const in_unit, Texture *in_texture) = 0;
        
        // vertex array
        virtual VertexArray *create_vertex_array(VertexArraySettings const &in_settings) = 0;
        virtual void destroy_vertex_array(VertexArray *in_vertexArray) = 0;

        // draw
        virtual void draw_points(VertexArray *in_vertexArray, uint32_t const in_first = 0, uint32_t const in_count = -1) = 0;
        virtual void draw_lines(VertexArray *in_vertexArray, uint32_t const in_first = 0, uint32_t const in_count = -1) = 0;
        virtual void draw_triangles(VertexArray *in_vertexArray, uint32_t const in_first = 0, uint32_t const in_count = -1) = 0;
    };
}