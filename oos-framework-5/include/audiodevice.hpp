/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    // ======================================================================================
    //
    //  AudioDevice
    //
    // ======================================================================================
    struct AudioDevice {
        //virtual AudioSample *create_sample() = 0;
        //virtual AudioStream *create_stream() = 0;

        //virtual void play_sample(AudioSample *in_sample) = 0;
        //virtual void play_stream(AudioStream *in_stream) = 0;
    };
}