/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <string>


namespace oos {
    // ======================================================================================
    //
    //  Window
    //
    // ======================================================================================
    struct Window {
        virtual void *handle() = 0;

        virtual int width() const = 0;
        virtual int height() const = 0;
        
        virtual std::string const &title() const = 0;
        virtual void title(std::string const &in_title) = 0;

        virtual void show() = 0;
    };
}