/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    /*
        bool is_little_endian() {
            int16_t  value = 0x1234;
            int8_t   *temp = (int8_t *)&temp;
            return(0x34 == *temp);
        }
        */

    // ======================================================================================
    //
    //  SystemDevice
    //
    // ======================================================================================
    struct SystemDevice {
        //virtual bool is_little_endian() = 0;
        /*
        virtual void scan_directory(std::string const &in_path, bool const in_recursive) = 0;

        virtual void create_directory(std::string const &in_path) = 0;
        virtual void delete_directory(std::string const &in_path) = 0;*/
    };
}